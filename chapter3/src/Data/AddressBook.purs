module Data.AddressBook where

import Prelude
import Control.Plus (empty)
import Data.List (List(..), filter, head)
import Data.Maybe (Maybe)

type Address =
  { street :: String
  , city :: String
  , state :: String
  }

type Entry =
  { firstName :: String
  , lastName :: String
  , address :: Address
  }

type AddressBook = List Entry

showEntry :: Entry -> String
showEntry e = e.lastName <> ", " <>
              e.firstName <> ": " <>
              showAddress e.address

showAddress :: Address -> String
showAddress a = a.street <> ", " <>
                a.city <> ", " <>
                a.state

emptyBook :: AddressBook
emptyBook = empty

insertEntry :: Entry -> AddressBook -> AddressBook
insertEntry e book = Cons e book

findEntry :: String -> String -> AddressBook -> Maybe Entry
findEntry firstName lastName = head <<< filter filterEntry
  where filterEntry :: Entry -> Boolean
        filterEntry entry = entry.firstName == firstName && entry.lastName == lastName
