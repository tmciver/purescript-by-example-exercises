module Lib where

import Prelude
import Math (pi)

circleArea :: Number -> Number
circleArea r = pi * r * r
